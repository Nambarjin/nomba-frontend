module.exports = {
  purge: [
    './pages/**/*.{js,ts,jsx,tsx}',
    './components/**/*.{js,ts,jsx,tsx}',
    './layouts/**/*.{js,ts,jsx,tsx}',
    './sections/**/*.{js,ts,jsx,tsx}',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        'main-orange': '#ffae2b',
        'main-gray': '#808080',
        'trans-white': 'rgba(255, 255, 255, 0.75)',
      },
      fontSize: {
        '2.5xl': '1.6875rem',
      },
      borderRadius: {
        '4xl': '3rem',
      },
      borderWidth: {
        1: '1px',
      },
      aspectRatio: {
        3.7: '3.7',
      },
      fontFamily: {
        main: 'Oz-Handicraft',
        decor: 'Propisi',
      },
      height: {
        hero: '58vh',
        38: '40rem',
      },
      maxWidth: {
        'screen-limit': '1920px',
        '8xl': '1300px',
      },
      width: {
        'card-12': '12ch',
        76: '19rem',
      },
      boxShadow: {
        '3xl': '3px 3px 12px rgba(0, 0, 0, 0.3)',
      },
      backgroundImage: (theme) => ({
        'wavy-orange': "url('/images/wavy-orange.png')",
        'wavy-white': "url('/images/wavy-white.png')",
      }),
    },
  },
  variants: {
    extend: {},
  },
  plugins: [require('@tailwindcss/aspect-ratio')],
};
