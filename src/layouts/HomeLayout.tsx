import Header from '../components/Header';
import Footer from '../components/Footer';

export default function HomeLayout({ children }) {
  return (
    <div className="w-full max-w-screen-limit mx-auto">
      <Header />
      {children}
      <Footer />
    </div>
  );
}
