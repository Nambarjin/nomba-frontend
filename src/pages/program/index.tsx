import React from 'react';
import Layout from '../../layouts/HomeLayout';
import SEO from '../../components/SEO';
import Hero from '../../sections/ProgramHero'

function Program() {
  return (
    <>
      <SEO title="Номтой бамбарууш | Хөтөлбөр" />
      <Layout>
        <Hero />
      </Layout>
    </>
  );
}

export default Program;
