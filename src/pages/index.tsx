import Layout from '../layouts/HomeLayout';
import SEO from '../components/SEO';
import Hero from '../sections/Hero';
import Curriculum from '../sections/Curriculum';
import Feedback from '../sections/Feedback';
import VideoGallery from '../sections/VideoGallery';

export default function Home() {
  return (
    <>
      <SEO title="Номтой бамбарууш | Нүүр" />
      <Layout>
        <Hero />
        <Curriculum />
        <Feedback />
        <VideoGallery />
      </Layout>
    </>
  );
}
