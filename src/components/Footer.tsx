import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faFacebookSquare,
  faInstagram,
} from '@fortawesome/free-brands-svg-icons';

function Footer() {
  return (
    <footer className=" bg-main-gray py-7 md:py-14">
      <div className="text-white flex justify-center items-center flex-wrap">
        <div className="flex py-3 md:pr-24 md:pl-9 items-center lg:border-r-1 border-gray-500">
          <img className="w-36 h-14 object-contain mr-6" src="/images/gegee-white.png" />
          <div className="flex pl-6 border-l-1 border-white h-8">
            <FontAwesomeIcon
              icon={faFacebookSquare}
              className="w-6 cursor-pointer"
            />
            <FontAwesomeIcon
              icon={faInstagram}
              className="w-6 cursor-pointer ml-2"
            />
          </div>
        </div>
        <div className="flex py-3 md:pr-24 md:pl-9 items-center lg:border-r-1 border-gray-500">
          <img className="w-36 h-14 object-contain mr-6" src="/images/gegee-school.png" />
          <div className="flex pl-6 border-l-1 border-white h-8">
            <FontAwesomeIcon
              icon={faFacebookSquare}
              className="w-6 cursor-pointer"
            />
            <FontAwesomeIcon
              icon={faInstagram}
              className="w-6 cursor-pointer ml-2"
            />
          </div>
        </div>
        <div className="flex py-3 md:pr-24 pl-9 items-end md:items-center font-main text-2xl">
          <p className="cursor-pointer mr-5">БАЙРШИЛ</p>
          <p className="cursor-pointer">ХОЛБОО БАРИХ</p>
        </div>
      </div>
    </footer>
  );
}

export default Footer;
