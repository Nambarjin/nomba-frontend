import Image from 'next/image';
import Link from 'next/link';
import { useRouter } from 'next/router';

const menuList = [
  {
    path: '/program',
    label: 'Хөтөлбөр',
  },
  {
    path: '/book',
    label: 'Ном',
  },
  {
    path: '/advice',
    label: 'Зөвөлгөө',
  },
  {
    path: '/#',
    label: 'Бүтээлч булан',
  },
  {
    path: '/#',
    label: 'Танин мэдэхүй',
  },
  {
    path: '/#',
    label: 'Монгол ахуй',
  },
  {
    path: '/#',
    label: 'Онлайн дэлгүүр',
  },
  {
    path: '/#',
    label: 'Бидний тухай',
  },
];

function Header() {
  const router = useRouter();
  return (
    <header className="sticky top-0 z-10 bg-white">
      <div className="max-w-7xl mx-auto flex justify-between px-2 py-2 2xl:mt-11 md:mb-4">
        <div className="w-32 h-10 md:w-52 md:h-16 relative">
          <Link href="/">
            <a>
              <Image
                src="/images/logo.png"
                alt="nomBa-logo"
                layout="fill"
                objectFit="contain"
                loading="eager"
              />
            </a>
          </Link>
        </div>
        <nav className="hidden 2xl:flex flex-1 justify-between ml-14">
          <ul className="flex text-3xl font-main justify-between flex-1 items-center">
            {menuList.map((item) => {
              return (
                <li
                  className={`hover:text-main-orange cursor-pointer duration-200 ${
                    router.pathname.startsWith(item.path)
                      ? 'text-main-orange'
                      : 'text-main-gray'
                  }`}
                  key={item.label}
                >
                  <Link href={item.path}>
                    <a>{item.label}</a>
                  </Link>
                </li>
              );
            })}
            <li>
              <button className="bg-main-orange text-white py-1 px-4 rounded-lg uppercase focus:outline-none">
                Нэвтрэх
              </button>
            </li>
          </ul>
        </nav>
        <button
          type="button"
          className="inline-flex justify-center text-main-orange focus:outline-none 2xl:hidden"
          aria-controls="mobile-menu"
          aria-expanded="false"
        >
          <span className="sr-only">Open main menu</span>
          <svg
            className="block h-10 w-10"
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
            aria-hidden="true"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth="2"
              d="M4 6h16M4 12h16M4 18h16"
            />
          </svg>
          <svg
            className="hidden h-10 w-10"
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
            aria-hidden="true"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth="2"
              d="M6 18L18 6M6 6l12 12"
            />
          </svg>
        </button>
      </div>
    </header>
  );
}

export default Header;
