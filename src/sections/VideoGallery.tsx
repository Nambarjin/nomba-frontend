import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faChevronLeft,
  faChevronRight,
  faEye,
  faStar,
} from '@fortawesome/free-solid-svg-icons';

const galleryData = [
  {
    views: '308',
    title: 'Хүүхэдтэйгээ хамт гацуурын чимэглэл урлах',
    postedAt: '2021.01.06',
    rating: '8.9',
  },
  {
    views: '178',
    title: 'Оймсоор хүүхэлдэй хийх',
    postedAt: '2021.01.06',
    rating: '6.5',
  },
  {
    views: '69',
    title: 'Зайрмагны модоор зааж хийж, чимэглэх',
    postedAt: '2021.01.06',
    rating: '7.1',
  },
];

function VideoGallery() {
  return (
    <div className="bg-wavy-white md:bg-cover transform -translate-y-10 z-10 pt-20 mb-20">
      <div className="w-full lg:px-40 px-5 mx-auto">
        <div className="flex justify-end text-3xl  md:text-5xl">
          <div className="bg-main-orange py-3 px-5 rounded-2xl text-white cursor-pointer  font-main shadow-3xl">
            Бүх бичлэгийг үзэх
          </div>
          <div className="md:py-2 px-4 md:px-5 text-main-gray cursor-pointer border-4 border-main-gray flex justify-center items-center rounded-2xl ml-2 opacity-60">
            <FontAwesomeIcon icon={faChevronLeft} className="w-4 md:w-6" />
          </div>
          <div className="md:py-2 px-4 md:px-5 text-main-gray cursor-pointer border-4 border-main-gray flex justify-center items-center rounded-2xl ml-2 opacity-60">
            <FontAwesomeIcon icon={faChevronRight} className="w-4 md:w-6" />
          </div>
        </div>
        <div className="max-w-7xl mx-auto grid gap-14 mt-16 grid-col-1 md:grid-cols-2 lg:grid-cols-3 text-center">
          {galleryData.map(({ views, title, postedAt, rating }) => {
            return (
              <div className="cursor-pointer" key={title}>
                <div className=" relative rounded-4xl overflow-hidden">
                  <img
                    className="object-cover"
                    src="/images/video-placeholder.png"
                  />
                  <div className="absolute bottom-0 right-0 bg-main-gray rounded-tl-4xl w-24 h-14 text-white flex justify-center items-center">
                    <FontAwesomeIcon icon={faEye} className="w-6" />
                    <div className="ml-1">{views}</div>
                  </div>
                </div>
                <div className="text-left px-7  text-main-gray">
                  <div className="text-4xl font-main mt-3.5">{title}</div>
                  <div className="font-main my-3.5 text-3xl opacity-50">
                    {postedAt}
                  </div>
                  <div className="text-4xl flex items-center">
                    <FontAwesomeIcon
                      icon={faStar}
                      className="w-12 text-yellow-400 mr-2"
                    />
                    {rating}
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
}
export default VideoGallery;
