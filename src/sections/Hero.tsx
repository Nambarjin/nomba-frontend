import SwiperCore, { Navigation, Autoplay } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import Image from 'next/image';
import 'swiper/swiper-bundle.min.css';

SwiperCore.use([Navigation, Autoplay]);

const SlidesData = [
  {
    src: '/images/slide-1.jpg',
  },
  {
    src: '/images/slide-2.jpg',
  },
];

function Hero() {
  return (
    <div className="hero aspect-w-4 aspect-h-3 md:aspect-w-2 md:aspect-h-1 xl:aspect-w-3.7 xl:aspect-h-1">
      <div className="h-full w-full">
        <Swiper
          loop
          slidesPerView={1}
          navigation
          autoplay={{ delay: 4000 }}
          className="h-full w-full"
        >
          {SlidesData.map((slide) => {
            return (
              <SwiperSlide key={slide.src}>
                <Image
                  src={slide.src}
                  layout="fill"
                  objectFit="cover"
                  priority
                />
                <div className="max-w-7xl mx-auto relative h-full">
                  <button className="z-10 absolute max-w-7xl mx-auto bottom-0 bg-main-orange text-white py-1 px-5 rounded-t-3xl capitalize focus:outline-none ml-4 lg:ml-28 font-main text-2xl md:text-4xl">
                    Дэлгэрэнгүй...
                  </button>
                </div>
              </SwiperSlide>
            );
          })}
        </Swiper>
      </div>
    </div>
  );
}

export default Hero;
