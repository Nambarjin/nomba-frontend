import Image from 'next/image';

const lessonsData = [
  {
    src: '/images/1.png',
    name: 'Монгол хэл',
  },
  {
    src: '/images/2.png',
    name: 'Англи хэл',
  },
  {
    src: '/images/3.png',
    name: 'Хятад хэл',
  },
  {
    src: '/images/4.png',
    name: 'Казак хэл',
  },
  {
    src: '/images/5.png',
    name: 'Япон хэл',
  },
  {
    src: '/images/6.png',
    name: 'Уйгаржин',
  },
  {
    src: '/images/7.png',
    name: 'Mongolian in English',
  },
  {
    src: '/images/8.png',
    name: 'Тоо тоолол',
  },
  {
    src: '/images/9.png',
    name: 'Дуу шүлэг',
  },
];

function Curriculum() {
  return (
    <div className="md:pb-12">
      <div className="flex justify-center items-center py-9 text-3xl md:text-5xl">
        <div className="font-decor">Сургалт</div>
        <div className="bg-main-orange text-white py-3 px-5 rounded-2xl uppercase focus:outline-none font-main">
          Хөтөлбөр
        </div>
      </div>
      <div className="max-w-7xl mx-auto flex flex-wrap lg:px-24 justify-center items-center lg:space-x-8 space-y-1 space-x-1">
        {lessonsData.map((lesson) => {
          return (
            <div key={lesson.name} className="px-7 pt-10 pb-5  cursor-pointer hover:bg-main-orange duration-300 rounded-3xl flex flex-col justify-center items-center">
              <img src={lesson.src} className="w-16 md:w-24" />
              <p className="mt-5 font-main text-main-gray text-2xl md:text-4xl text-center w-card-12">
                {lesson.name}
              </p>
            </div>
          );
        })}
      </div>
    </div>
  );
}

export default Curriculum;
