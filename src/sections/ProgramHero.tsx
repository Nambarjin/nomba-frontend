import Image from 'next/image';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronRight } from '@fortawesome/free-solid-svg-icons';

const lessonData = [
  {
    name: 'Монгол хэл',
  },
  {
    name: 'Англи хэл',
  },
  {
    name: 'Хятад хэл',
  },
  {
    name: 'Казак хэл',
  },
  {
    name: 'Уйгаржин',
  },
  {
    name: 'Mongolian in English',
  },
  {
    name: 'Тоо тоолол',
  },
  {
    name: 'Дуу шүлэг',
  },
];

function ProgramHero() {
  return (
    <>
      <div className="hero aspect-w-4 aspect-h-3 md:aspect-w-2 md:aspect-h-1 xl:aspect-w-3.7 xl:aspect-h-1">
        <div className="h-full w-full">
          <Image
            src="/images/slide-2.jpg"
            layout="fill"
            objectFit="cover"
            priority
          />
        </div>
      </div>
      <div className="max-w-8xl mx-auto px-1">
        <div className="flex justify-between font-main text-main-gray text-3xl pt-4">
          {lessonData.map((lesson) => {
            return (
              <a className="hover:text-main-orange cursor-pointer duration-500">
                {lesson.name}
              </a>
            );
          })}
        </div>
        <div className="flex text-main-gray opacity-60 font-main text-2.5xl mt-6 mb-12">
          <span>Нүүр Хуудас</span>
          <FontAwesomeIcon icon={faChevronRight} className="w-2 mx-4" />
          <span>Хөтөлбөр</span>
          <FontAwesomeIcon icon={faChevronRight} className="w-2 mx-4" />
          <span>Монгол хэл</span>
        </div>
        <div className="px-20 flex justify-between mb-32 flex-wrap">
          <div className="w-76 h-38 border-main-gray border-2 border-opacity-70 transform hover:scale-110 duration-500 rounded-4xl pt-12 flex flex-col">
            <div className="px-12">
              <div className="aspect-w-2 aspect-h-3">
                <img src="/images/10.png" className="object-contain" />
              </div>
            </div>
            <div className="py-4 pl-12 pr-8 flex-grow flex flex-col font-main text-main-gray">
              <div className="group flex justify-between my-1 items-baseline">
                <div className="text-4xl">Түвшин 1</div>
                <a className="bg-main-orange text-white px-4 rounded-lg focus:outline-none text-2xl opacity-0 group-hover:opacity-100 cursor-pointer">
                  Үзэх
                </a>
              </div>
              <div className="group flex justify-between my-1 items-baseline">
                <div className="text-4xl">Түвшин 2</div>
                <a className="bg-main-orange text-white px-4 rounded-lg focus:outline-none text-2xl opacity-0 group-hover:opacity-100 cursor-pointer">
                  Үзэх
                </a>
              </div>
              <div className="group flex justify-between my-1 items-baseline">
                <div className="text-4xl">Түвшин 3</div>
                <a className="bg-main-orange text-white px-4 rounded-lg focus:outline-none text-2xl opacity-0 group-hover:opacity-100 cursor-pointer">
                  Үзэх
                </a>
              </div>
            </div>
          </div>
          <div className="w-76 h-38 border-main-gray border-2 border-opacity-70 transform hover:scale-110 duration-500 rounded-4xl pt-12 flex flex-col">
            <div className="px-12">
              <div className="aspect-w-2 aspect-h-3">
                <img src="/images/11.png" className="object-contain" />
              </div>
            </div>
            <div className="py-4 pl-12 pr-8 flex-grow flex flex-col font-main text-main-gray">
              <div className="group flex justify-between my-1 items-baseline">
                <div className="text-4xl">Түвшин 1</div>
                <a className="bg-main-orange text-white px-4 rounded-lg focus:outline-none text-2xl opacity-0 group-hover:opacity-100 cursor-pointer">
                  Үзэх
                </a>
              </div>
              <div className="group flex justify-between my-1 items-baseline">
                <div className="text-4xl">Түвшин 2</div>
                <a className="bg-main-orange text-white px-4 rounded-lg focus:outline-none text-2xl opacity-0 group-hover:opacity-100 cursor-pointer">
                  Үзэх
                </a>
              </div>
              <div className="group flex justify-between my-1 items-baseline">
                <div className="text-4xl">Түвшин 3</div>
                <a className="bg-main-orange text-white px-4 rounded-lg focus:outline-none text-2xl opacity-0 group-hover:opacity-100 cursor-pointer">
                  Үзэх
                </a>
              </div>
              <div className="group flex justify-between my-1 items-baseline">
                <div className="text-4xl">Түвшин 4</div>
                <a className="bg-main-orange text-white px-4 rounded-lg focus:outline-none text-2xl opacity-0 group-hover:opacity-100 cursor-pointer">
                  Үзэх
                </a>
              </div>
              <div className="group flex justify-between my-1 items-baseline">
                <div className="text-4xl">Цагаан толгой</div>
                <a className="bg-main-orange text-white px-4 rounded-lg focus:outline-none text-2xl opacity-0 group-hover:opacity-100 cursor-pointer">
                  Үзэх
                </a>
              </div>
            </div>
          </div>
          <div className="w-76 h-38 border-main-gray border-2 border-opacity-70 transform hover:scale-110 duration-500 rounded-4xl pt-12 flex flex-col">
            <div className="px-12">
              <div className="aspect-w-2 aspect-h-3">
                <img src="/images/12.png" className="object-contain" />
              </div>
            </div>
            <div className="py-4 pl-12 pr-8 flex-grow flex flex-col font-main text-main-gray">
              <div className="group flex justify-between my-1 items-baseline">
                <div className="text-4xl">Түвшин 1</div>
                <a className="bg-main-orange text-white px-4 rounded-lg focus:outline-none text-2xl opacity-0 group-hover:opacity-100 cursor-pointer">
                  Үзэх
                </a>
              </div>
              <div className="group flex justify-between my-1 items-baseline">
                <div className="text-4xl">Түвшин 2</div>
                <a className="bg-main-orange text-white px-4 rounded-lg focus:outline-none text-2xl opacity-0 group-hover:opacity-100 cursor-pointer">
                  Үзэх
                </a>
              </div>
              <div className="group flex justify-between my-1 items-baseline">
                <div className="text-4xl">Түвшин 3</div>
                <a className="bg-main-orange text-white px-4 rounded-lg focus:outline-none text-2xl opacity-0 group-hover:opacity-100 cursor-pointer">
                  Үзэх
                </a>
              </div>
              <div className="group flex justify-between my-1 items-baseline">
                <div className="text-4xl">Түвшин 4</div>
                <a className="bg-main-orange text-white px-4 rounded-lg focus:outline-none text-2xl opacity-0 group-hover:opacity-100 cursor-pointer">
                  Үзэх
                </a>
              </div>
              <div className="group flex justify-between my-1 items-baseline">
                <div className="text-4xl">Түвшин 5</div>
                <a className="bg-main-orange text-white px-4 rounded-lg focus:outline-none text-2xl opacity-0 group-hover:opacity-100 cursor-pointer">
                  Үзэх
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default ProgramHero;
