import SwiperCore, { Navigation, Autoplay, Pagination } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/swiper-bundle.min.css';

SwiperCore.use([Navigation, Autoplay, Pagination]);

function Feedback() {
  return (
    <div className="relative bg-wavy-orange md:bg-cover py-12 px-5">
      <div className="flex text-white justify-center items-center  pb-8 mt-5 text-3xl md:text-5xl">
        <div className="font-decor">Суралцагчдын</div>
        <div className="bg-main-orange py-3 px-5 rounded-2xl uppercase font-main shadow-3xl">
          Сэтгэгдэл
        </div>
      </div>
      <img className="h-2/3 absolute bottom-0 md:left-40" src="/images/teddy.png" />
      <Swiper
        slidesPerView={1}
        spaceBetween={15}
        pagination={{ clickable: true }}
        loop
        className="max-w-2xl mx-auto"
        style={{ paddingBottom: '3rem' }}
        breakpoints={{
          640: {
            slidesPerView: 2,
          },
        }}
      >
        <SwiperSlide>
          <div className="bg-trans-white md:bg-white rounded-4xl h-80 px-12 py-4 text-main-orange font-main text-xl flex">
            <p className="self-end">124</p>
          </div>
        </SwiperSlide>
        <SwiperSlide>
          <div className="bg-trans-white md:bg-white rounded-4xl h-80 px-12 py-4 text-main-orange font-main text-xl flex">
            <p className="self-end">86</p>
          </div>
        </SwiperSlide>
      </Swiper>
    </div>
  );
}

export default Feedback;
